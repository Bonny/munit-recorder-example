%dw 2.0
import * from dw::test::Asserts
---
payload must equalTo([
  {
    "order_id": 1,
    "product_id": 201,
    "customer_id": 301,
    "status": "SENT",
    "dt": "2021-01-12"
  },
  {
    "order_id": 2,
    "product_id": 201,
    "customer_id": 301,
    "status": "DELIVERED",
    "dt": "2021-01-12"
  },
  {
    "order_id": 3,
    "product_id": 202,
    "customer_id": 303,
    "status": "PREPARING",
    "dt": "2021-01-12"
  },
  {
    "order_id": 4,
    "product_id": 203,
    "customer_id": 303,
    "status": "PREPARING",
    "dt": "2021-01-12"
  }
])